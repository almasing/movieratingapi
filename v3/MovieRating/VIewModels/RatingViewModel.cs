﻿namespace MovieRating.Helpers
{
    public class RatingViewModel
    {
        public string movieOrTvShowGuid { get; set; }
        public string userGuid { get; set; }
        public int rate { get; set; }
    }
}