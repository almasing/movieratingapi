﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MovieRating.Helpers;
using MovieRating.Models;
using MovieRating.ViewModels;

namespace MovieRating.Controllers
{
    public class MoviesAndShowsController : ApiController
    {
        private MovieRatingEntities1 db = new MovieRatingEntities1();

        // GET: api/MoviesAndShows/5
        [ResponseType(typeof(MoviesAndShow))]
        [Route("api/moviesandshows/{start}/{end}/{type}/{queryString}")]
        public async Task<IHttpActionResult> GetMoviesAndShow(int start, int end, int type, string queryString)
        {
            MoviesAndShowsHelper moviesAndShowHelper = new MoviesAndShowsHelper();

            string userGuid = moviesAndShowHelper.getUserGuidFromTheHeader(this.Request);
            IEnumerable <MovieOrShowViewModel> moviesAndShows = moviesAndShowHelper.GetMoviesAndShows(userGuid, db, start, end, type)
              .Where(mas => mas.overview.ToLower().Contains(queryString.ToLower())
                    || mas.title.ToLower().Contains(queryString));

            if (moviesAndShows == null)
            {
                return NotFound();
            }

            return Ok(moviesAndShows);
        }

        // GET: api/MoviesAndShows/5
        [ResponseType(typeof(MoviesAndShow))]
        [Route("api/moviesandshows/{start}/{end}/{type}")]
        public async Task<IHttpActionResult> GetMoviesAndShow(int start, int end, int type)
        {
            MoviesAndShowsHelper moviesAndShowHelper = new MoviesAndShowsHelper();

            string userGuid = moviesAndShowHelper.getUserGuidFromTheHeader(this.Request);
            IEnumerable<MovieOrShowViewModel> moviesAndShows = moviesAndShowHelper.GetMoviesAndShows(userGuid, db, start, end, type);

            if (moviesAndShows == null)
            {
                return NotFound();
            }

            return Ok(moviesAndShows);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MoviesAndShowExists(int id)
        {
            return db.MoviesAndShows.Count(e => e.Id == id) > 0;
        }
    }
}