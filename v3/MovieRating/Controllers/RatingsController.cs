﻿using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MovieRating.Helpers;
using MovieRating.Models;

namespace MovieRating.Controllers
{
    public class RatingsController : ApiController
    {
        private MovieRatingEntities1 db = new MovieRatingEntities1();

        // POST: api/Ratings
        [ResponseType(typeof(RatingViewModel))]
        public async Task<IHttpActionResult> PostRating(RatingViewModel ratingViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Rating ratingModel;

            if (!RatingExists(ratingViewModel))
            {
                ratingModel = new Rating
                {
                    Rate = ratingViewModel.rate,
                    FKMovieOrShow = db.MoviesAndShows.Where(g => g.Guid == ratingViewModel.movieOrTvShowGuid).Select(r => r.Id).First(),
                    FKUser = db.Users.Where(g => g.Guid == ratingViewModel.userGuid).Select(r => r.Id).First()
                };
                

                db.Ratings.Add(ratingModel);
                await db.SaveChangesAsync();

                return Ok();
            }

            // if it does, get the rating from the DB
            ratingModel = db.Ratings
                .Where(r => r.User.Guid == ratingViewModel.userGuid && r.MoviesAndShow.Guid == ratingViewModel.movieOrTvShowGuid)
                .Select(r => r).FirstOrDefault();

            // then update it's value
            ratingModel.Rate = ratingViewModel.rate;

            // then mark it as modified
            db.Entry(ratingModel).State = EntityState.Modified;

            // then try saving it
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest("There was an issue with the request");
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RatingExists(RatingViewModel rating)
        {
            return db.Ratings.Count(r => r.User.Guid == rating.userGuid && r.MoviesAndShow.Guid == rating.movieOrTvShowGuid) > 0;
        }
    }
}