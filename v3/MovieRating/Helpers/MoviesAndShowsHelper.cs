﻿using MovieRating.Models;
using MovieRating.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace MovieRating.Helpers
{
    public class MoviesAndShowsHelper
    {
        public IEnumerable<MovieOrShowViewModel> GetMoviesAndShows(string userGuid, MovieRatingEntities1 db, int start, int end, int type)
        {

            IEnumerable<MovieOrShowViewModel> moviesAndShows = db.MoviesAndShows
                .Where(mas => mas.Type.Id == type)
                .OrderByDescending(mas => mas.Ratings.Average(r => r.Rate))
                .AsEnumerable()
                .Select(mas => new MovieOrShowViewModel
                {
                    title = mas.Title,
                    overview = mas.Overview,
                    poster = String.Format("https://image.tmdb.org/t/p/w185{0}", mas.Poster),
                    releaseDate = mas.ReleaseDate.ToString().Substring(0, 10), // I should have used DATE type in the db but since
                                                                               // I realised that after I enterd the data I opted
                                                                               // just to cut the string short
                    type = mas.Type.Id,
                    rating = db.Ratings.Where(r => r.FKMovieOrShow == mas.Id).Average(r => r.Rate),
                    userRating = db.Ratings.Where(r => r.FKMovieOrShow == mas.Id && r.User.Guid == userGuid).Select(r => r.Rate).FirstOrDefault(),
                    cast = db.Casts.Where(c => c.FKMovieOrShow == mas.Id).Select(c => c.Actor.FirstName + " " + c.Actor.LastName).ToList(),
                    guid = mas.Guid
                })
                .Skip(start).Take(end - start);

            return moviesAndShows;
        }

        public string getUserGuidFromTheHeader(HttpRequestMessage request)
        {
            // Getting the user guid from the request header.
            System.Net.Http.Headers.HttpRequestHeaders headers = request.Headers;
            string userGuid = string.Empty;
            if (headers.Contains("userGuid"))
            {
                userGuid = headers.GetValues("userGuid").First();
            }

            return userGuid;
        }
    }
}