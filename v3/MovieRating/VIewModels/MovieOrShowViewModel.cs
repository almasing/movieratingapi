﻿using System.Collections.Generic;

namespace MovieRating.ViewModels
{
    public class MovieOrShowViewModel
    {
        public string title { get; set; }
        public string overview { get; set; }
        public string poster { get; set; }
        public string releaseDate { get; set; }
        public double rating { get; set; }
        public int userRating { get; set; }
        public int type { get; set; }
        public List<string> cast { get; set; }
        public string guid { get; set; }
    }
}