﻿namespace MovieRating.ViewModels
{
    public class ActorViewModel
    {
        public string name { get; set; }
        public string profileImg { get; set; }
    }
}